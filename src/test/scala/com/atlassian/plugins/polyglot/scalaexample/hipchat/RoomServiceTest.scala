package com.atlassian.plugins.polyglot.scalaexample.hipchat

import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import java.util.Date

@RunWith(classOf[JUnitRunner])
class RoomServiceTest extends FlatSpec {


  "An XML room list" should "be properly deserialized into a list of rooms" in {
    val rooms = Room.listFromXml(stubRoomList)
    assert(rooms.length == 2, "There should be 2 rooms in the list")
    assert(rooms.head.id == 54420, "Ensure the first room has the correct id")
  }

  "A single xml room of the list" should "be deserialized into a Room instance" in {
    val room = Room.fromXml(stubRoom).get
    assert(room.id == 54420, "Ensure the id is correct")
    assert(room.created == new Date(1331178335 * 1000L))
  }

  "A full room" should "be properly deserialized and should contain the participants" in {
    val room = Room.fromXml(stubFullRoom).get
    assert(room.id == 5, "Ensure the id is correct")
    assert(room.participants.length == 2, "There should be 2 participants")
  }

  "Room history" should "be properly deserialized" in {
    val messages = Message.listFromXml(stubRoomHistory)
    assert(messages.length == 3, "There should be 3 chat messages")

    val message = messages.head
    assert(message.from.isDefined, "The first message should have an author")
    assert(message.from.get.name == "Klaus Tester")
  }

  val stubRoom = <room>
    <room_id>54420</room_id> <name>Confluence</name> <topic></topic> <last_active>0</last_active> <created>1331178335</created> <owner_user_id>83977</owner_user_id> <is_archived>0</is_archived> <is_private>1</is_private> <xmpp_jid>19107_confluence@conf.hipchat.com</xmpp_jid>
  </room>
  val stubRoomList = <rooms>
    <room>
      <room_id>54420</room_id> <name>Confluence</name> <topic></topic> <last_active>0</last_active> <created>1331178335</created> <owner_user_id>83977</owner_user_id> <is_archived>0</is_archived> <is_private>1</is_private> <xmpp_jid>19107_confluence@conf.hipchat.com</xmpp_jid>
    </room> <room>
      <room_id>54419</room_id> <name>no comp</name> <topic>Welcome! Send this link to coworkers who need accounts: https://www.hipchat.com/invite/19107/24b698ebdbb0e43873f75ca201fc5418</topic> <last_active>0</last_active> <created>1331178290</created> <owner_user_id>83977</owner_user_id> <is_archived>0</is_archived> <is_private>0</is_private> <xmpp_jid>19107_no_comp@conf.hipchat.com</xmpp_jid>
    </room>
  </rooms>
  val stubFullRoom = <room>
    <room_id>5</room_id>
    <name>Ops</name>
    <topic>Chef is so awesome.</topic>
    <last_active>1269020400</last_active>
    <created>1269010311</created>
    <is_archived>0</is_archived>
    <is_private>1</is_private>
    <owner_user_id>5</owner_user_id>
    <participants>
      <participant>
        <user_id>5</user_id>
        <name>Garret Heaton</name>
      </participant>
      <participant>
        <user_id>1</user_id>
        <name>Chris Rivers</name>
      </participant>
    </participants>
      <guest_access_url/>
    <xmpp_jid>5_ops@conf.hipchat.com</xmpp_jid>
  </room>


  val stubRoomHistory = <messages>
    <message>
      <date>2010-11-19T15:48:19-0800</date>
      <from>
        <name>Klaus Tester</name>
        <user_id>110</user_id>
      </from>
      <message>Good morning! This is a regular message.</message>
    </message>
    <message>
      <date>2010-11-19T15:49:44-0800</date>
      <from>
        <name>Garret Heaton</name>
        <user_id>10</user_id>
      </from>
      <file>
        <name>Screenshot.png</name>
        <size>141909</size>
        <url>http://uploads.hipchat.com/xxx/Screenshot.png</url>
      </file>
      <message>This is a file upload</message>
    </message>
    <message>
      <date>2010-11-19T16:13:40-0800</date>
      <from>
        <name>Deploy Bot</name>
        <user_id>api</user_id>
      </from>
      <message>This message is sent via the API so the user_id is 'api'.</message>
    </message>
  </messages>
}
