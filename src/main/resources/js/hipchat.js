(AJS.toInit(function ($) {

    var throbber, spinner;
    /**
     * Hide the loading indicator.
     */
    var hideThrobber = function () {
        if (spinner) {
            spinner();
            spinner = null;
        }
        throbber.addClass("hidden");
    };


    var container = $("#hipchat-rooms");


    if (container.length) {
        throbber = $("<div class='throbber'></div>");
        container.append(throbber);
        spinner = Raphael.spinner(throbber[0], 16, "#666");
    }
    var url = AJS.contextPath() + "/rest/hipchat/1.0/room";
    $.get(url, function (data) {
        var html = Confluence.Templates.HipChat.Rooms.list({rooms:data});
        hideThrobber();
        $("#hipchat-rooms").html(html);
    });

}))(AJS.$);
