(AJS.toInit(function ($) {
    var macroContainer = $("#hipchat-room-history");
    var roomId = macroContainer.attr("data-room-id");
    var url = AJS.contextPath() + "/rest/hipchat/1.0/room/" + roomId;
    $.get(url, function (data) {
        var html = Confluence.Templates.HipChat.Rooms.history({messages:data});
        macroContainer.html(html);
    });
}))(AJS.$);
