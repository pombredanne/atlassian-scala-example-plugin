package com.atlassian.plugins.polyglot.scalaexample

import org.slf4j.LoggerFactory


trait Logging {
  protected lazy val log = LoggerFactory.getLogger(this.getClass);

  def debugLog[A](prefix:String)(f: => A):A = {
    val r = f
    log.debug("%s: %s" format(prefix, r))
    r
  }
}
