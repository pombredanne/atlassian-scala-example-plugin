package com.atlassian.plugins.polyglot.scalaexample.macro

import com.atlassian.plugins.polyglot.scalaexample._

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.templaterenderer.TemplateRenderer
import java.io.StringWriter
import com.atlassian.plugins.polyglot.scalaexample.TemplateEngineAware
import com.atlassian.confluence.macro.Macro.{OutputType, BodyType}
import java.util.{Map => JMap}
import com.atlassian.confluence.pages.thumbnail.Dimensions
import com.atlassian.confluence.macro.{DefaultImagePlaceholder, EditorImagePlaceholder, Macro}

/**
 * Macro that provides the initial data required to show the recent messages in a HipChat chat room.
 */
class HipChatMacro(val templateRenderer:TemplateRenderer, val pluginSettings: CustomPluginSettings) extends Macro
  with TemplateEngineAware with EditorImagePlaceholder {

  def execute(params: JMap[String, String], body: String, context: ConversionContext) = {
    val writer = new StringWriter()
    pluginSettings.authenticationToken match {
      case Some(token) => {
        val context = Map[String,AnyRef]("remoteUser" -> AuthenticatedUserThreadLocal.getUser, "roomId" -> (params.get("roomId") getOrElse "0"))
        render(writer, "/templates/macro/hipchat-history.vm", context)
      }
      case None => render(writer, "/templates/macro/hipchat-token-missing.vm", Map[String,AnyRef]())
    }

    writer.toString
  }

  def getBodyType = BodyType.NONE

  def getOutputType = OutputType.BLOCK

  val imagePath = "/download/resources/com.atlassian.plugins.polyglot.scala.scala-example-plugin/img/hipchat-macro-placeholder.png"
  def getImagePlaceholder(params: JMap[String, String], ctx: ConversionContext) = new DefaultImagePlaceholder(imagePath, new Dimensions(85, 25), false);
}
