package com.atlassian.plugins.polyglot.scalaexample

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal
import com.atlassian.user.User

trait UserAware {
  def user:Option[User] = AuthenticatedUserThreadLocal.getUser
}
