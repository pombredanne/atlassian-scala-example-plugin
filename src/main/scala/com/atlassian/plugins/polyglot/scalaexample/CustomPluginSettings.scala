package com.atlassian.plugins.polyglot.scalaexample

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory

case class Token(value:String)

trait CustomPluginSettings {
  def authenticationToken:Option[Token]
  def updateToken(apiToken:Token)
}

class DefaultPluginSettings(pluginSettingsFactory: PluginSettingsFactory) extends CustomPluginSettings {

  private val PLUGIN_STORAGE_KEY = classOf[CustomPluginSettings].getName
  private val HIPCHAT_AUTH_TOKEN_KEY = "hipchat-auth-token"

  def authenticationToken = {
    pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY).get(HIPCHAT_AUTH_TOKEN_KEY) map {token =>
      Token(token.toString)
    }
  }

  def updateToken(apiToken: Token) {
    apiToken map {token =>
      pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY).put(HIPCHAT_AUTH_TOKEN_KEY, token.value)
    }
  }
}
