package com.atlassian.plugins.polyglot.scalaexample.hipchat

import com.atlassian.plugins.polyglot.scalaexample._
import org.codehaus.jackson.annotate.{JsonAutoDetect, JsonCreator, JsonProperty}
import reflect.BeanProperty
import xml.Node
import java.util.Date
import java.text.SimpleDateFormat
import org.codehaus.jackson.map.annotate.JsonSerialize
import org.codehaus.jackson.JsonGenerator
import org.codehaus.jackson.map.{SerializerProvider, JsonSerializer}
import annotation.target.{getter, param, beanGetter}

// In a real plugin it wouldn't be strictly necessary to model the HipChat entities
// A simple proxy that returns the JSON as returned by the HipChat API would be sufficient.
// For demonstration purposes we assume that it is necessary to define the entities here.

class CustomDateSerializer extends JsonSerializer[Date] {
  lazy val formatter = new SimpleDateFormat("MM-dd-yyyy")// TODO Wrap in TL, use Locale based format
  def serialize(date: Date, gen: JsonGenerator, sp: SerializerProvider) {
    gen.writeString(formatter.format(date));
  }
}

class CustomOptionSerializer extends JsonSerializer[Option[_]] {
  def serialize(opt: Option[_], gen: JsonGenerator, sp: SerializerProvider) {
    opt match {
      case Some(x) => gen.writeObject(x)
      case None => gen.writeNull()
    }
  }
}

// See http://www.scala-lang.org/api/current/scala/annotation/target/package.html
// for the usage of meta-annotations
object HipChatEntity {
  type SerializeWith = JsonSerialize @beanGetter
}

import HipChatEntity._

sealed abstract class HipChatEntity

@JsonAutoDetect
case class Person @JsonCreator()(@BeanProperty id: String, @BeanProperty name: String) extends HipChatEntity // id can be an Int or "api" or "guest"

@JsonAutoDetect
case class Room @JsonCreator()(@BeanProperty id: Int, @BeanProperty name: String,
                               @BeanProperty topic: String, lastActive: Date,
                               created: Date, isArchived: Boolean, isPrivate: Boolean,
                               participants: Seq[Person]) extends HipChatEntity

@JsonAutoDetect
case class Message @JsonCreator()(@SerializeWith(using = classOf[CustomDateSerializer]) @BeanProperty date:Date,
                                  @SerializeWith(using = classOf[CustomOptionSerializer]) @BeanProperty from:Option[Person],
                                  @BeanProperty message:String,
                                  file:Option[File]) extends HipChatEntity
@JsonAutoDetect
case class File @JsonCreator()(@BeanProperty name:String,
                 @BeanProperty size:Int,
                 @BeanProperty url:String) extends HipChatEntity

// Additional methods on the Room method to map the HipChat API XML to our internal domain objects
object Room {
  /** Unmarshall XML */
  def fromXml(node: scala.xml.Node): Option[Room] = scala.xml.Utility.trimProper(node) match {
    case elem@ <room>{_*}</room> =>
      Room(
        (elem \ "room_id").text.toInt,
        (elem \ "name").text,
        (elem \ "topic").text,
        (elem \ "last_active").text.toLong,
        (elem \ "created").text.toLong,
        (elem \ "is_archived").text,
        (elem \ "is_private").text,
        (elem \ "participants") flatMap {
          Person.listFromXml(_)
        }
      )
    case _ => None
  }

  /** Unmarshall XML -> sequence of rooms */
  def listFromXml(rooms: scala.xml.Node): Seq[Room] = rooms match {
    case <rooms>{xs @ _*}</rooms> => xs flatMap { Room.fromXml(_) }
    case _ => Nil
  }

  // 0 -> false, 1 -> true
  implicit def asBoolean(str: String): Boolean = if (str.toInt == 0) false else true

  // Long (timestamp in UTC) to Date
  implicit def asDate(seconds: Long): Date = new Date(seconds * 1000L)
}

object Message {
  /**Unmarshall XML -> Seq[Person] */
  def listFromXml(people: Node): Seq[Message] = people match {
    case <messages>{xs @ _*}</messages> => xs flatMap { Message.fromXml(_) }
    case _ => Nil
  }

  /**Unmarshall XML -> Option[Person] */
  def fromXml(node: Node): Option[Message] = scala.xml.Utility.trimProper(node) match {
    case elem @ <message>{_*}</message> =>
      Message(
        (elem \ "date").text,
        Person.fromXml((elem \ "from") head),
        (elem \ "message").text,
        None
      )
    case _ => None
  }

  implicit def asDate(tmstmp:String):Date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz").parse(tmstmp)
}

object Person {
  /**Unmarshall XML -> Seq[Person] */
  def listFromXml(people: Node): Seq[Person] = people match {
    case <participants>{xs @ _*}</participants> => xs flatMap { Person.fromXml(_) }
    case _ => Nil
  }

  /**Unmarshall XML -> Option[Person] */
  def fromXml(node: Node): Option[Person] = scala.xml.Utility.trimProper(node) match {
    case elem @ <participant>{_*}</participant> =>
      Person(
        (elem \ "user_id").text,
        (elem \ "name").text
      )
    case elem @ <from>{_*}</from> =>
      Person(
        (elem \ "user_id").text,
        (elem \ "name").text
      )
    case _ => None
  }
}

object HipChat
