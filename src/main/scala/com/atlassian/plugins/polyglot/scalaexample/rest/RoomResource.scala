package com.atlassian.plugins.polyglot.scalaexample.rest

import com.atlassian.plugins.polyglot.scalaexample.hipchat.RoomService
import javax.ws.rs._

import core.{Response, MediaType}
import scala.collection.JavaConverters._

@Path("room")
@Consumes(Array(MediaType.APPLICATION_JSON))
@Produces(Array(MediaType.APPLICATION_JSON))
class RoomResource(roomService:RoomService) {

  def errorHandler(e:Exception) = Response.serverError().entity(e.getLocalizedMessage).build()
  def success[A](entity:java.util.Collection[A]):Response = Response.ok(entity).build()

  @GET
  def list = roomService.list.fold(
    e => errorHandler(e),
    s => success(s.asJava)
  )

  @GET
  @Path("{id : \\d+}")
  def roomHistory(@PathParam("id") roomId:Int) = roomService.roomHistory(roomId).fold(
    e => errorHandler(e),
    s => success(s.asJava)
  )
}


