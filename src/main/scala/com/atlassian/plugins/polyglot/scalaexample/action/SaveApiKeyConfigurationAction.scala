package com.atlassian.plugins.polyglot.scalaexample.action

import com.atlassian.confluence.core.ConfluenceActionSupport
import com.opensymphony.xwork.Action
import reflect.BeanProperty
import com.atlassian.xwork.RequireSecurityToken
import com.atlassian.plugins.polyglot.scalaexample.{Token, CustomPluginSettings, UserAware}
import org.apache.commons.lang.StringUtils

/**XWork Action ot show the current configuration screen */
class SaveApiKeyConfigurationAction
  extends ConfluenceActionSupport // extend a class provided by the Confluence API
  with UserAware {

  /** The generated setter will be invoked by XWork with the request parameter named hipChatAuthToken */
  @BeanProperty var hipChatAuthToken: String = _

  /**Check whether the user is permitted to view the configuration */
  override def isPermitted = permissionManager.isConfluenceAdministrator(user.get)

  override def validate() {
    if (StringUtils.isBlank(hipChatAuthToken)) {
      addActionError(getText("hipchat.token.form.invalidtokenerror"), Nil)
    }
  }

  @RequireSecurityToken(true)
  override def execute() = {
    customPluginSettings.updateToken(Token(hipChatAuthToken))
    Action.SUCCESS
  }


  /** Setter injection */
  @BeanProperty var customPluginSettings: CustomPluginSettings  = _
  // We have to use setter injection if we don't use the defaultStack
  // See https://jira.atlassian.com/browse/CONF-23137
}
