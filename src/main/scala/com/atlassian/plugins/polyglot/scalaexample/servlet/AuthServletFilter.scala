package com.atlassian.plugins.polyglot.scalaexample.servlet

import com.atlassian.plugins.polyglot.scalaexample._
import javax.servlet._
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal
import com.atlassian.user.User
import http.{HttpServletRequest, HttpServletResponse}


class AuthServletFilter extends Filter {

  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
    if (user.isDefined) {
      chain.doFilter(request, response)
    } else {
      response.asInstanceOf[HttpServletResponse] map { httpServletRequest =>
        httpServletRequest.sendRedirect(loginAction(request.asInstanceOf[HttpServletRequest]))
      }
    }
  }

  def user: Option[User] = AuthenticatedUserThreadLocal.getUser

  def loginAction(request: HttpServletRequest) = "%s/login.action" format request.getContextPath

  def destroy() {} // NOOP

  def init(filterConfig: FilterConfig) {} // NOOP
}
