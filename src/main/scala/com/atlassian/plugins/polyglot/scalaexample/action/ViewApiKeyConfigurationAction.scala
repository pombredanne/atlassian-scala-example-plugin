package com.atlassian.plugins.polyglot.scalaexample.action

import com.atlassian.confluence.core.ConfluenceActionSupport
import com.atlassian.confluence.security.PermissionManager
import com.opensymphony.xwork.Action
import com.atlassian.plugins.polyglot.scalaexample.{CustomPluginSettings, UserAware}

/**XWork Action ot show the current configuration screen */
class ViewApiKeyConfigurationAction(permissionManager: PermissionManager,
                                    customPluginSettings: CustomPluginSettings)
  extends ConfluenceActionSupport // extend a class provided by the Confluence API
  with UserAware { // Custom trait internal to the plugin

  private var successFullUpdate = false

  /**Check whether the user is permitted to view the configuration */
  override def isPermitted = permissionManager.isConfluenceAdministrator(user.get)

  /**Follow get/set style accessor method naming convention to make usage from within the template easier */
  def getHipChatAuthToken = customPluginSettings.authenticationToken map {_.value} getOrElse ""

  override def execute() = Action.SUCCESS

  def setResult(result:String) {
    successFullUpdate = if("success" == result) true else false
  }

  def isSuccessFullUpdate = successFullUpdate
}
