package com.atlassian.plugins.polyglot.scalaexample

import java.io.Writer
import com.atlassian.templaterenderer.TemplateRenderer
import collection.JavaConverters._

trait TemplateEngineAware {
  val templateRenderer: TemplateRenderer

  def render(writer: Writer, templatePath: String, context: Map[String, AnyRef]) {
    templateRenderer.render(templatePath, context.asJava, writer)
  }
}
