package com.atlassian.plugins.polyglot

package object scalaexample {
  /** Converts a function () => Any into a Runnable */
  implicit def function2Runnable(f: () => Any) = new Runnable {
    def run() {
      f()
    }
  }

  /**
   * Turn an A into an Option if required. Nice for automatically wrapping nulls in an option.
   */
  implicit def toOption[A](any:A): Option[A] = Option(any)
}