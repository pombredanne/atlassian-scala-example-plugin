package com.atlassian.plugins.polyglot.scalaexample.servlet

import com.atlassian.templaterenderer.TemplateRenderer
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal
import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import com.atlassian.plugins.polyglot.scalaexample.TemplateEngineAware

class HipChatViewServlet(val templateRenderer:TemplateRenderer)
  extends HttpServlet
  with TemplateEngineAware {

  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    resp.setContentType("text/html")
    val context = Map("remoteUser" -> AuthenticatedUserThreadLocal.getUser)
    render(resp.getWriter, "/templates/servlet/hipchat.vm", context)
  }
}
