Scala Example Plugin
====================

This is an example Alassian plugin written in Scala.

The plugin contains examples of different Confluence plugin module types:

* Servlet
* REST resources and serialization of Scala case classes using Jackson
* Confluence Macro
* XWork Action

![Example macro](http://ssaasen.bitbucket.org/atlassian-scala-example-plugin/images/scala-plugin-1.png)
![Example macro](http://ssaasen.bitbucket.org/atlassian-scala-example-plugin/images/scala-plugin-3.png)


In addition to implementation examples this project is set up to properly build
and compile the Scala classes, run Scala based tests and shows different ways
of bundling the Scala runtime library.

---

*This plugin is for demonstration purposes. Hence it is incomplete and not
production ready.*

---

Development
===========

During development the following commands can be used to start the host
application and to install/update the plugin (See the [Plugin SDK Documentation](https://developer.atlassian.com/display/DOCS/Atlassian+Plugin+SDK+Documentation) for further information):

Start the host application in debug mode:

    atlas-debug


Start the shell (run <tt>pi</tt> in the shell to install/update the plugin,
alternatively enable the FastDev plugin and do a hard reload (CMD+Shift+R on
Mac) to automatically package and install the updated plugin).

    atlas-cli

The maven-scala-plugin defines a set of goals that are valuable during
development.

To compile the main and test scala source directory in continuously, run:

    mvn scala:cc

To start the Scala REPL/console with all classes of the project (and its
dependencies) available, run:

    mvn scala:console -Pdev


Deployment
==========

Adding the Scala runtime library to a plugin by simply embedding it in its
entirety adds around 8MB to the size of the plugin.

Depending on the use case this can be acceptable but it is preferable to keep
the size of the final plugin jar down.

There are a couple of options to achieve this.

Deploy the Scala runtime library separately
-------------------------------------------

A Scala runtime library with a proper OSGi manifest can be deployed into the
plugin container of the application.

The plugin itself simply imports the proper packages and does not need to
bundle the library itself:

            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>3.8</version>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Import-Package>
                            scala*;version="${scala.version}",
                            *
                        </Import-Package>

For Scala 2.8 and 2.9 the following plugins available from
https://plugins.atlassian.com bundle the Scala runtime for consumption by other
plugins:


* https://plugins.atlassian.com/plugins/com.atlassian.confluence.plugins.scala-provider-plugin
* https://plugins.atlassian.com/plugins/com.atlassian.confluence.plugins.scala-2.9-provider-plugin


Shrink the size of the bundled library using ProGuard
-----------------------------------------------------

Another option is to shrink the library by removing unused code using a tool
like [ProGuard](http://proguard.sourceforge.net/).

This example project contains a ProGuard configuration file and a Maven profile
that shrinks the size of the final plugin from around 8.7MB to ~ 700KB.

Run:

    mvn clean package -Dproduction

To create the plugin with the required Scala library class files embedded.

Deployment options at a glance
------------------------------

The three different deployment options are demonstrated in this plugin by using
different profiles:

Include the full <tt>scala-library</tt> dependency in the plugin:

    mvn package -Pbundle

Include a shrinked <tt>scala-library</tt> dependency in the plugin

    mvn package -Pproduction

Depend on the <tt>scala-library</tt> being deployed as a separate plugin
(e.g. using the (Scala provider plugin)[https://plugins.atlassian.com/plugins/com.atlassian.confluence.plugins.scala-2.9-provider-plugin]

    mvn package

    mvn package -Pdefault


Resources
=========

* [HipChat API](https://www.hipchat.com/docs/api)
* [http://proguard.sourceforge.net/](http://proguard.sourceforge.net/)



